<?php
// class definition
class Person {
    // define properties
    public $name;
    public $weight;
    public $age;
    public $colour;
    // constructor
    public function __construct() {
        $this->age = 0;
        $this->weight = 10;
        $this->colour = "white";
    }
    public function ran($miles){
        echo $this->name." ran ".$miles." miles without food or water.<br>";
    }
    //destructor
    public function __destruct(){
        echo $this->name." still weighed ".$this->weight." units and died.<br>";
    }
}
$thomas = new Person;
$thomas->name = "Salabir";
$thomas->ran(10);
$thomas->ran(218);
?>




